from django.db import models
from pika import PlainCredentials

from information_bot_web import settings as s
from exchange import ExchangeConnection


class Question(models.Model):
    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы от пользователей'

    user = models.ForeignKey(
        "users.User",
        on_delete=models.SET_DEFAULT,
        null=True,
        default=None
    )

    question = models.TextField(
    )

    answer = models.TextField(
        null=True,
    )

    sent = models.BooleanField(
        verbose_name='Отвечено',
        default=False,
    )

    def save(self, *args, **kwargs):
        if not self.sent:
            self.sent = True
            exchange = ExchangeConnection(
                s.RABBITMQ_HOST,
                s.RABBITMQ_PORT,
                PlainCredentials(s.RABBITMQ_USER, s.RABBITMQ_PASS)
            )
            exchange.declare_queue('sos_response')

            exchange.publish(
                [self.user.tg_id, self.answer],
                'sos_response'
            )
            exchange.close_connection()
        super(Question, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.question)
