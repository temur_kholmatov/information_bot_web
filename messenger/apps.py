from django.apps import AppConfig


class MessagesConfig(AppConfig):
    name = 'messenger'
    verbose_name = 'Мессенджер'

