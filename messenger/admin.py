from django.contrib import admin
from messenger.models import *


class MessageConfig(admin.ModelAdmin):
    list_display = ('text', 'date_time', 'valid', 'sent',)
    readonly_fields = ('valid', 'sent',)
    list_filter = ('sent', 'valid')

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.sent:
            return 'text', 'date_time', 'valid', 'sent'
        return self.readonly_fields


admin.site.register(Message, MessageConfig)


class MessageTeamsConfig(admin.ModelAdmin):
    list_display = ('get_teams', 'text', 'date_time', 'valid', 'sent',)
    search_fields = ()
    readonly_fields = ('teams', 'valid', 'sent',)
    list_filter = ('sent', 'valid')

    def get_teams(self, obj):
        return ", ".join([str(t) for t in obj.teams.all()])

    get_teams.short_description = 'Команды'

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.sent:
            return 'text', 'date_time', 'teams', 'valid', 'sent'
        return self.readonly_fields


admin.site.register(MessageTeams, MessageTeamsConfig)


class MessageCompetitionsConfig(admin.ModelAdmin):
    list_display = ('get_competitions', 'date_time', 'text', 'valid', 'sent',)
    search_fields = ()
    readonly_fields = ('competitions', 'valid', 'sent',)
    list_filter = ('competitions', 'sent', 'valid',)

    def get_competitions(self, obj):
        return ", ".join([str(t) for t in obj.competitions.all()])

    get_competitions.short_description = 'Соревнования'

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.sent:
            return 'text', 'date_time', 'competitions', 'valid', 'sent'
        return self.readonly_fields


admin.site.register(MessageCompetitions, MessageCompetitionsConfig)


class MessageRegionsConfig(admin.ModelAdmin):
    list_display = ('get_regions', 'text', 'date_time', 'valid', 'sent',)
    search_fields = ()
    readonly_fields = ('regions', 'valid', 'sent',)
    list_filter = ('regions', 'sent', 'valid', )

    def get_regions(self, obj):
        return ", ".join([str(t) for t in obj.regions.all()])

    get_regions.short_description = 'Регионы'

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.sent:
            return 'text', 'date_time', 'regions', 'valid', 'sent'
        return self.readonly_fields


admin.site.register(MessageRegions, MessageRegionsConfig)


class MessageStatusConfig(admin.ModelAdmin):
    list_display = ('status', 'text', 'date_time', 'valid', 'sent',)
    search_fields = ('status',)
    readonly_fields = ('valid', 'sent',)
    list_filter = ('status', 'sent', 'valid',)

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.sent:
            return 'text', 'date_time', 'status', 'valid', 'sent'
        return self.readonly_fields


admin.site.register(MessageStatus, MessageStatusConfig)


class PersonalMessageConfig(admin.ModelAdmin):
    list_display = ('user', 'text', 'date_time', 'valid', 'sent',)
    search_fields = ('user',)
    readonly_fields = ('user', 'valid', 'sent',)
    list_filter = ('sent', 'valid')

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.sent:
            return 'text', 'date_time', 'user', 'valid', 'sent'
        return self.readonly_fields


admin.site.register(PersonalMessage, PersonalMessageConfig)
