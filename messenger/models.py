from datetime import datetime

import pytz
from django.db import models
from pika import PlainCredentials

from exchange import ExchangeConnection
from information_bot_web import settings as s
from information_bot_web.configs import *
from information_bot_web.settings import TIME_ZONE


def microservice_send_message(message):
    exchange = ExchangeConnection(
        s.RABBITMQ_HOST,
        s.RABBITMQ_PORT,
        PlainCredentials(s.RABBITMQ_USER, s.RABBITMQ_PASS)
    )
    exchange.declare_queue('broadcast')
    exchange.publish(
        message,
        'broadcast'
    )
    exchange.close_connection()


class Message(models.Model):
    class Meta:
        verbose_name = 'Сообщение всем'
        verbose_name_plural = 'Сообщения всем'

    text = models.TextField(
        verbose_name='Текст',
        null=True
    )

    date_time = models.DateTimeField(
        verbose_name='Время отправки',
        default=None,
        null=True,
        blank=True,
        help_text=help_text_for_date_time
    )

    sent = models.BooleanField(
        verbose_name='Отправлено',
        default=False,
    )

    valid = models.BooleanField(
        verbose_name='Передано в отправку',
        default=False,
    )

    def save(self, *args, **kwargs):
        if self.date_time is None:
            self.date_time = datetime.now(pytz.timezone(TIME_ZONE))
        super(Message, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.text)


class MessageRegions(models.Model):
    class Meta:
        verbose_name = 'Сообщение по региону'
        verbose_name_plural = 'Сообщения по регионам'

    text = models.TextField(
        verbose_name='Текст',
        null=True
    )

    date_time = models.DateTimeField(
        verbose_name='Время отправки',
        default=None,
        null=True,
        blank=True,
        help_text=help_text_for_date_time
    )

    sent = models.BooleanField(
        verbose_name='Отправлено',
        default=False,
    )

    regions = models.ManyToManyField(
        "users.Region",
        related_name='regions',
        verbose_name='Регионы',
    )

    valid = models.BooleanField(
        verbose_name='Передано в отправку',
        default=False,
    )

    def save(self, send=True, *args, **kwargs):
        if send and self.date_time is None:
            self.date_time = datetime.now(pytz.timezone(TIME_ZONE))
        super(MessageRegions, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.text)


class MessageCompetitions(models.Model):
    class Meta:
        verbose_name = 'Сообщение по соревнованию'
        verbose_name_plural = 'Сообщения по соревнованию'

    text = models.TextField(
        verbose_name='Текст',
        null=True
    )

    date_time = models.DateTimeField(
        verbose_name='Время отправки',
        default=None,
        null=True,
        blank=True,
        help_text=help_text_for_date_time
    )

    sent = models.BooleanField(
        verbose_name='Отправлено',
        default=False,
    )

    valid = models.BooleanField(
        verbose_name='Передано в отправку',
        default=False,
    )

    competitions = models.ManyToManyField(
        "competitions.Competition",
        related_name='competitions',
        verbose_name='Соревнования',
    )

    def __str__(self):
        return str(self.text)

    def save(self, send=True, *args, **kwargs):
        if send and self.date_time is None:
            self.date_time = datetime.now(pytz.timezone(TIME_ZONE))
        super(MessageCompetitions, self).save(*args, **kwargs)


class MessageTeams(models.Model):
    class Meta:
        verbose_name = 'Сообщение команде'
        verbose_name_plural = 'Сообщения по командам'

    text = models.TextField(
        verbose_name='Текст',
        null=True
    )

    date_time = models.DateTimeField(
        verbose_name='Время отправки',
        default=None,
        null=True,
        blank=True,
        help_text=help_text_for_date_time
    )

    sent = models.BooleanField(
        verbose_name='Отправлено',
        default=False,
    )

    valid = models.BooleanField(
        verbose_name='Передано в отправку',
        default=False,
    )

    teams = models.ManyToManyField(
        "users.Team",
        related_name='teams',
        verbose_name='Команды',
    )

    def save(self, send=True, *args, **kwargs):
        if send and self.date_time is None:
            self.date_time = datetime.now(pytz.timezone(TIME_ZONE))
        super(MessageTeams, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.text)


class MessageStatus(models.Model):
    class Meta:
        verbose_name = 'Сообщение по статусу'
        verbose_name_plural = 'Сообщения по статусам'

    text = models.TextField(
        verbose_name='Текст',
        null=True
    )

    date_time = models.DateTimeField(
        verbose_name='Время отправки',
        default=None,
        null=True,
        blank=True,
        help_text=help_text_for_date_time
    )

    sent = models.BooleanField(
        verbose_name='Отправлено',
        default=False,
    )

    valid = models.BooleanField(
        verbose_name='Передано в отправку',
        default=False,
    )

    status = models.IntegerField(
        choices=STATUS_CHOICES,
        verbose_name='Статус',
        null=True
    )

    def save(self, *args, **kwargs):
        if self.date_time is None:
            self.date_time = datetime.now(pytz.timezone(TIME_ZONE))
        super(MessageStatus, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.text)


class PersonalMessage(models.Model):
    class Meta:
        verbose_name = 'Персональное сообщение'
        verbose_name_plural = 'Персональные сообщения'

    text = models.TextField(
        verbose_name='Текст',
        null=True
    )

    date_time = models.DateTimeField(
        verbose_name='Время отправки',
        default=None,
        null=True,
        blank=True,
        help_text=help_text_for_date_time
    )

    sent = models.BooleanField(
        verbose_name='Отправлено',
        default=False,
    )

    valid = models.BooleanField(
        verbose_name='Передано в отправку',
        default=False,
    )

    user = models.ForeignKey(
        "users.User",
        on_delete=models.SET_DEFAULT,
        verbose_name='Участник',
        default=None,
        null=True
    )

    def save(self, send=True, *args, **kwargs):
        if send and self.date_time is None:
            self.date_time = datetime.now(pytz.timezone(TIME_ZONE))
        super(PersonalMessage, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.text)
