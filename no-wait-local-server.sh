#!/usr/bin/env bash
docker stack rm info_bots_web
echo "Wait"
sleep 15
docker container prune -f

./build-local.sh
docker stack deploy --compose-file docker_config/compose.yml info_bots_web

echo "Completed"
