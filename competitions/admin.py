from django.conf.urls import url
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse

from .models import *
from django.utils.html import format_html
from messenger.models import MessageCompetitions
from information_bot_web.settings import HOST
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class CompetitionCategoryResource(resources.ModelResource):
    class Meta:
        model = CompetitionCategory


class CompetitionCategoryConfig(ImportExportModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    resource_class = CompetitionCategoryResource


admin.site.register(CompetitionCategory, CompetitionCategoryConfig)


class CompetitionResource(resources.ModelResource):
    class Meta:
        model = Competition

    def before_import_row(self, row, **kwargs):
        category = row['category']
        if category is not None:
            if not str(category).isnumeric():
                row['category'] = CompetitionCategory.objects.get(name=category).id


class CompetitionConfig(ImportExportModelAdmin):
    list_display = ('name', 'category', 'meal', 'user_action',)
    search_fields = ('name',)
    readonly_fields = ('user_action',)
    list_filter = ('category',)
    resource_class = CompetitionResource

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<competition_id>.+)/sendmessage/$',
                self.admin_site.admin_view(self.process_action),
                name='send-competition-message',
            ),
        ]
        return custom_urls + urls

    def user_action(self, obj):
        return format_html(
            '<a class="button" href="{}"/">Написать</a>',
            reverse('admin:send-competition-message', args=[obj.id])
        )

    def process_action(self, request, competition_id):
        competition = Competition.objects.get(id=competition_id)
        msg = MessageCompetitions()
        msg.save(send=False)
        msg.competitions.add(competition)
        return HttpResponseRedirect(f"http://{HOST}/admin/messenger/messagecompetitions/{msg.id}/")

    def make_action(self, request, queryset):
        msg = MessageCompetitions()
        msg.save(send=False)
        for competition in queryset:
            msg.competitions.add(competition)
        return HttpResponseRedirect(f"http://{HOST}/admin/messenger/messagecompetitions/{msg.id}/")

    def make_open_first(self, request, queryset):
        queryset.update(fst_round_active=True)

    def make_open_second(self, request, queryset):
        queryset.update(snd_round_active=True)

    def make_open_winners(self, request, queryset):
        queryset.update(winners_active=True)

    def make_close_first(self, request, queryset):
        queryset.update(fst_round_active=False)

    def make_close_second(self, request, queryset):
        queryset.update(snd_round_active=False)

    def make_close_winners(self, request, queryset):
        queryset.update(winners_active=False)

    actions = [make_action, make_open_first, make_open_second, make_open_winners, make_close_first, make_close_second,
               make_close_winners]
    make_action.short_description = 'Написать сообщение'
    make_open_first.short_description = "Открыть 1 раунд"
    make_open_second.short_description = "Открыть 2 раунд"
    make_open_winners.short_description = "Открыть победителей"
    make_close_first.short_description = "Закрыть 1 раунд"
    make_close_second.short_description = "Закрыть 2 раунд"
    make_close_winners.short_description = "Закрыть победителей"
    user_action.short_description = 'Сообщение'
    user_action.allow_tags = True


admin.site.register(Competition, CompetitionConfig)


class ParserResource(resources.ModelResource):
    class Meta:
        model = Parser


class ParserConfig(ImportExportModelAdmin):
    list_display = ('name', 'list_name', 'range', 'columns',)
    search_fields = ('name',)
    resource_class = ParserResource


admin.site.register(Parser, ParserConfig)
