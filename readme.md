# README
InfoBot is a Telegram Bot created for events such like olympiads and competitions held in Innopolis University. The project is developed by a group of students. InfoBot is used to register users, show information about competitions in comfortable way, provide user with organisational information and etc.

InfoBot consists of two parts:
- Bot Microservices
- Main Services

Here we review **Main Services** part. To see **Bot Microservices** part visit another project's [repository][repo].

# Installation Process
Install [Docker][docker] and [Docker Compose][dock_comp].

The project is created using Django and supports only admin mode.
Run **start-local-server.sh** to install and start web service container in Docker on localhost. This script also establishes [RabbitMQ][rabbit] and [PostgreSQL][postgres]. RabbitMQ is used to schedule and redirect tasks to microservices, while PostgreSQL is used as a database. You can use **no-wait-local-server.sh** to start web service only if RabbitMQ and PostgreSQL containers are already created.

# Import Data
Open the Django admin page.
Instead of adding model objects manually, you can import a list of objects from **.xls, .xlsx, .csv** file (better use .xls or .xlsx).

Here is the order of models and the file formats' representation:
1. Import objects to **Info** application. You can import files to models Питание, Проживание, Транспорт, Атташе in any order.

- Model Питание. File Format:

| id | name | info_1_day | info_2_day | info_3_day |
| ------ | ------ | ------ | ------ | ------ |
| 31 | Meal 1 | Can leave empty | Can leave empty | Can leave empty |
| 12 | Meal 2 | Can leave empty | Can leave empty | Can leave empty |

- Model Проживание. File Format:

| id | name | info |
| ------ | ------ | ------ |
| 31 | Housing 1 | Can leave empty |
| 45 | Housing 2 | Can leave empty |

- Model Транспорт. File Format:

| id | name | transfer_1 | shuttle_1 | transfer_2 | shuttle_2 | transfer_3 | shuttle_3 | taxi |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| 15 | Transport 1 | Can leave empty | Can leave empty | Can leave empty | Can leave empty | Can leave empty | Can leave empty | Can leave empty | 

- Model Атташе. File Format:

| id | name | info |
| ------ | ------ | ------ |
| 22 | Attache 1 | Can leave empty |
| 24 | Attache 2 | Can leave empty |

2. Import objects to **Competitions** application. You can import files to models Категория Соревнования, Парсер in any order. Afterward, import file to model Соревнование.
- Model Категория Соревнования:

| id | name |
| --- | --- |
| 3 | Competition Category 1 |

- Model Парсер. This model is used to parse Google sheets using data: *list_name* - name of a list containing table, *range* - a range of data to be read (without column names), *columns* - line containing column names separated by comma (,). A link to the sheet will be loaded in model Соревнование (see next). File Format:

| id | name | list_name | range | columns |
| --- | --- | --- | --- | --- |
| 3 | Parser 1 | List 1 | A1:C | column1_name,column2_name,column3_name |

- Model Соревнование. This model creates competition objects containing 2 rounds and 1 winners section by getting several data: *category* - foreign key to object of model Категория Соревнования (put the **name** of object, not **id**), *meal*, *..._parser* fields get **ids** to foreign keys (see competitions/admin.py for more details), *link* - a link to the Google sheet containing information about the results of this competition. File Format:

|id |	name |	category |	meal |	link |	fst_round_active |	fst_round_name |	fst_round_parser |	snd_round_active |	snd_round_name |	snd_round_parser |	winners_active |	winners_name |	winners_parser |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 6 | Example Competition | Competition Category 1 | 31 | https://docs.google.com/spreadsheets/d/Wi2n1-jNWeOFIEBWOIkN7H05Jzlv1l1O42DkqNno/ | True | Round 1 | 3 | False | Round 2 | 3 | False | Winners | 3 |

3. Import objects to **Users** application. Import files to models in this order Регионы, Команды, Участники.
- Model Регионы. The data imported includes such fields like: *code* - a code number of the region, *housing, transport, attache* - **ids** of foreign key objects of models Проживание, Транспорт, Атташе, respectively.

| id | name | code | housing | transport | attache |
| --- | --- | --- | --- | --- | --- |
| 5 | Region 1 | 50 | 45 | 15 | 22 |

- Model Команды. **Important:** the import of file in this model is implemented in different way and is adapted for a special file format: *ID* - id of a trainer that will be added to model Участники automatically and will join the current team like a member as soon as the team will be created, *Полное имя* -full name of the trainer, *Регион* - the **name** of a region of the trainer, *Регламент* - id and name of competition of a team, *Код* - code of the team (not used in importing), *Статус* - status of a team (does not import teams with status starting with "Отклонена"). File Format:

| ID | Полное имя | Регион | Регламент | Код | Статус |
| --- | --- | --- | --- | --- | --- |
| 13434 | Name Surname | Region 1 | 6. Example Competition | Can be empty | Принята организатором |

- Model Участники. **Important:** the import of file in this model is implemented in different way and is adapted for a special file format: *ID* - id of a team, *Регламент* - id and name of competition of the team (not used in importing), *ID команды* - id of a team that the participant will join automatically (the participant will **not** be imported in case the team does not exist), *Полное имя* - full name of the participant, *Регион* - name of the region (not used in importing since the region of the participant is taken from his trainer's personal data imported previously). File Format:

| ID | Регламент | ID команды | Полное имя | Регион |
| --- | --- | --- | --- | --- |
| 4969 | 6. Example Competition | 13434 | Name Surname1 | Region 1 |
| 4970 | 6. Example Competition | 13434 | Name Surname2 | Region 1 |

There are also other models where you can add objects:
- In Files application you can load files containing additional information. *Notice:* you should point a user who will get these files initially to upload files directly to Telegram servers and save file ids.
- After the users are registered, you can send them messages using Messages application.
- You can answer questions from users in Questions application.
- Application Ratings is used for storing data about images to be send as results of competitions. **Warning:** do not add or change objects of this application's model.

After importing all neccessary data, you can install and start bot microservices in Docker from this [repository][repo].

[repo]: <https://gitlab.com/inno-olymp-bots/information_bot_web>
[docker]: <https://www.docker.com/>
[dock_comp]: <https://docs.docker.com/compose/>
[rabbit]: <https://www.rabbitmq.com/>
[postgres]: <https://www.postgresql.org/>
[gog_api]: <https://developers.google.com/sheets/api/quickstart/python>