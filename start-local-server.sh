#!/usr/bin/env bash

docker swarm init --advertise-addr 10.135.91.13
read -p "Would you like to continue? (y/n)" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then

docker-compose -f docker_config/compose-helpers.yml build
docker stack deploy --compose-file docker_config/compose-helpers.yml info_bots_main

echo "Wait"
sleep 15

docker-compose -f docker_config/compose.yml build
docker stack deploy --compose-file docker_config/compose.yml info_bots_web

echo "Completed"

fi
