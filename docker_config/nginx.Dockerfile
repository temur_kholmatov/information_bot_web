FROM nginx:latest

RUN mkdir -p /var/serve/media
RUN mkdir -p /var/serve/static
RUN mkdir -p /etc/letsencrypt


ADD ./docker_config/nginx.conf /etc/nginx/nginx.conf
